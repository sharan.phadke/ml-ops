import keras
import numpy as np
from keras.layers import Dense
from keras.layers import Flatten
from keras.utils import np_utils
from tensorflow.keras.models import Sequential, Model

X_train=np.load('x_train.npy')
y_train=np.load('y_train.npy')

model=Sequential()
model.add(Flatten(input_shape=(28,28,1)))
model.add(Dense(256,activation='relu'))
model.add(Dense(128,activation='relu'))
model.add(Dense(64,activation='relu'))
model.add(Dense(32,activation='relu'))
model.add(Dense(10,activation='softmax'))
model.compile(loss='sparse_categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
model.summary()

model.fit(x=X_train,y=y_train,batch_size=120,epochs=50)
model.save('model.h5')
