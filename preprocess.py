import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import sklearn
from sklearn.model_selection import train_test_split

path = "digit-recognizer/train.csv"
mnist = pd.read_csv(path)

X, y = mnist.drop(["label"],axis=1), mnist['label']

X_train,X_test,y_train,y_test = train_test_split(X,y,test_size = 0.2, random_state = 42)

I_x_train=np.array(X_train)
I_x_test=np.array(X_test)
I_y_train=np.array(y_train)
I_y_test=np.array(y_test)

X_train=I_x_train.reshape(-1,28,28,1)
X_test=I_x_test.reshape(-1,28,28,1)

X_train = X_train.astype('float32')/255
X_test = X_test.astype('float32')/255

np.save('x_train',X_train)  
np.save('y_train',y_train)
np.save('x_test',X_test)
np.save('y_test',y_test)