import keras
import numpy as np
model = keras.models.load_model('model.h5')
x_test=np.load('x_test.npy')
y_test=np.load('y_test.npy')

test_loss, test_acc = model.evaluate(x_test, y_test)
print(test_acc)